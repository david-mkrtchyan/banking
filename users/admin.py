from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from utils.models import Currencies

# Register your models here.

from .models import Users
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe

class UserAdminCustom(UserAdmin):


    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'ip_address', 'phone_number',)

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (
            _('Personal info'), {'fields': (
                'first_name', 'last_name', 'email', 'phone_number', 'country_code', 'pin', 'phone_verified',
            )}),
        # (_('Additional info'),
        #  {'fields': ()}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    def save_model(self, request, obj, form, change):
        obj.save()

        if change == False:
            Currencies.createModel(obj.id)

        super(UserAdminCustom, self).save_model(request, obj, form, change)



admin.site.register(Users, UserAdminCustom)
