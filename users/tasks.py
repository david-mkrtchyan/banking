from __future__ import absolute_import
from utils.models import EmailTemplate
from banking.settings import EMAIL_HOST_USER
import re
from django.core.mail import send_mail
from banking.settings import SITE_NAME
from banking.celery import app
from django.core import serializers


#@app.task(name='send_mail_by_template')
def send_mail_by_template(slug, objs, emailTo):
    template = EmailTemplate.checkslug(slug)

    if template:
       subject = template.subject
       description = template.description

       objsnew = []
       for obj in serializers.deserialize("json", objs):
           objsnew.append(obj.object)

       send_mail(changeContent(objsnew, subject), '', EMAIL_HOST_USER, emailTo, fail_silently=True,
                html_message=changeContent(objsnew, description))


def changeContent(objs, content):
    dictattr = {
        '[site_name]': SITE_NAME
    }

    for obj in objs:
        objectName = obj.__class__
        for i in objectName._meta.get_fields():
            if hasattr(obj, i.name):
                key = '[' + objectName.__name__.lower() + '.' + str(i.name) + ']'
                if key in dictattr:
                    key = '[owner.' + objectName.__name__.lower() + '.' + str(i.name) + ']'
                dictattr[key] = getattr(obj, i.name)

    rep = dict((re.escape(k), v) for k, v in dictattr.items())
    pattern = re.compile("|".join(rep.keys()))
    text = pattern.sub(lambda m: rep[re.escape(m.group(0))], content)

    return text
