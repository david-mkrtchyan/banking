from rest_framework.response import Response
from users.models import Users as User
from rest_framework import status
from rest_framework.views import APIView
from .serializers import UserSerializerRegister, TransactionSendSerializer, \
    CheckPinSerializer, ExchangeSerializer, LatestTransactionSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from users.functions import getUserData, checkPhoneNumber
from rest_framework_simplejwt.views import TokenViewBase
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.serializers import PasswordField
from rest_framework import serializers as rest_serializers, exceptions
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _


class UserRegister(APIView):

    def post(self, request, *args, **kwargs):
        serializer = UserSerializerRegister(data=request.data, request=request)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            if data:
                return Response(
                    {'success': False, 'message': 'Your account has been created! now you must verify your pin',
                     'next-step': True},
                    status=status.HTTP_200_OK)
            return Response({'success': False, 'message': '', 'next-step': False},
                            status=status.HTTP_200_OK)


class UserCurrencies(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        lists = []
        currencies = request.user.getAccounts
        if currencies:
            for i in request.user.getAccounts:
                data = {
                    'account_id': i.id,
                    'name': i.currency.name,
                    'currency_symbol': i.currency.symbol,
                    'balance': i.balance,
                }
                lists.append(data)

        return Response(lists, status=status.HTTP_200_OK)


class CurrentUser(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        return Response(getUserData(request.user), status=status.HTTP_200_OK)


class TokenObtainSerializer(rest_serializers.Serializer):
    username_field = User.USERNAME_FIELD

    default_error_messages = {
        'no_active_account': _('You have entered an incorrect phone number or password')
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields[self.username_field] = rest_serializers.CharField()
        self.fields['password'] = PasswordField()

    def validate(self, attrs):

        authenticate_kwargs = {
            self.username_field: attrs[self.username_field],
            'password': attrs['password'],
        }

        try:
            authenticate_kwargs['request'] = self.context['request']
        except KeyError:
            pass

        self.user = authenticate(**authenticate_kwargs)

        try:
            if not self.user:
                self.user = User.objects.filter(pin=attrs['password'], phone_number=attrs[self.username_field]).first()
        except:
            pass

        from axes.handlers.proxy import AxesProxyHandler
        lockedUser = AxesProxyHandler.is_locked(request=self.context['request'], credentials=authenticate_kwargs)

        if lockedUser:
            try:
                from users.serializers import sendMessageToPhone
                from utils.models import Variables
                from utils.seeds import Seeds
                from banking.settings import AXES_FAILURE_LIMIT

                failures = int(AxesProxyHandler.get_implementation().get_failures(request=self.context['request'],
                                                                                  credentials=authenticate_kwargs))
                if failures == AXES_FAILURE_LIMIT + 1:
                    phone_number_mes = Variables.byKey(Seeds.blocked_account)
                    if phone_number_mes:
                        sendMessageToPhone(attrs[self.username_field], phone_number_mes.value)
            except Exception as e:
                import logging
                logging.basicConfig(filename='/srv/banking/logging.log', level=logging.DEBUG)
                logging.error('log ' + str(e))

                
            raise exceptions.AuthenticationFailed(
                self.error_messages['no_active_account'],
                'no_active_account',
            )

        if self.user and not self.user.phone_verified:
            raise exceptions.AuthenticationFailed(
                {'detail': self.error_messages['no_active_account'], 'phone_verified': False},
                'no_active_account',
            )

        if self.user is None or not self.user.is_active:
            raise exceptions.AuthenticationFailed(
                self.error_messages['no_active_account'],
                'no_active_account',
            )

        return {}

    @classmethod
    def get_token(cls, user):
        raise NotImplementedError('Must implement `get_token` method for `TokenObtainSerializer` subclasses')


class TokenObtainPairSerializer(TokenObtainSerializer):
    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)

        verified_by_phone = True if self.user.phone_verified else False

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['verify_account'] = verified_by_phone

        if not verified_by_phone:
            data[
                'verify_account_message'] = 'You need to verify your phone before you can make transactions. Do you wish to do that now?'

        return data


class TokenObtainPairView(TokenViewBase):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = TokenObtainPairSerializer


class TransactionSend(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = TransactionSendSerializer(data=request.data, request=request)
        if serializer.is_valid(raise_exception=True):
            return Response({'success': True, 'message': 'Your transaction is successfully done'},
                            status=status.HTTP_200_OK)


class CheckPin(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = CheckPinSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            return Response({'success': True, 'message': 'Your pin is confirmed', 'verified': True},
                            status=status.HTTP_200_OK)


class CheckUserExist(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        return checkPhoneNumber(request)


class Exchange(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = ExchangeSerializer(data=request.data, request=request, getData=False)
        if serializer.is_valid(raise_exception=True):
            return Response({'success': True, 'message': ''},
                            status=status.HTTP_200_OK)


class ExchangeGetData(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = ExchangeSerializer(data=request.data, request=request, getData=True)
        if serializer.is_valid(raise_exception=True):

            return Response({'success': True, 'message': serializer.validated_data},
                            status=status.HTTP_200_OK)


class UserFeed(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        lists = []
        transactions = request.user.getUserTransactions()
        if transactions:
            transactions=transactions[0:4]
            for i in transactions:
                data = {
                    'message': i.message,
                    'amount': i.amount,
                    'timestamp': i.timestamp.strftime('%d %b %Y'),
                }

                if i.from_user_id == request.user.id:
                    data['user_name'] = f'To {str(i.to_user.first_name)} {str(i.to_user.last_name)}'
                    data['currency'] = i.from_account.currency.name
                    data['currency_symbol'] = i.from_account.currency.symbol
                else:
                    data['user_name'] = f'From {str(i.from_user.first_name)} {str(i.from_user.last_name)}'
                    data['currency'] =  i.to_account.currency.name
                    data['currency_symbol'] = i.to_account.currency.symbol

                lists.append(data)

        return Response({'success': True, 'message': lists},
                            status=status.HTTP_200_OK)


class LatestTransaction(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        serializer = LatestTransactionSerializer(data=request.data, request=request)
        if serializer.is_valid(raise_exception=True):
            return Response({'success': True, 'message': serializer.validated_data},
                            status=status.HTTP_200_OK)
