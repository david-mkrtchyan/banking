from django import forms
from users.models import Users as User
from django.utils.translation import gettext as _
from django.contrib.auth import (
    authenticate, get_user_model, password_validation
)
from django.contrib import messages
from django.utils.text import capfirst
from intl_tel_input.widgets import IntlTelInputWidget
from django_countries.fields import CountryField
from django_countries.widgets import CountrySelectWidget
from django_countries import countries
from django.core.validators import MaxValueValidator

UserModel = get_user_model()


class UserRegisterForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two pin fields didn't match."),
    }
    password = forms.IntegerField(
        label=_("Password"),
        required=True,
    )

    password2 = forms.IntegerField(
        label=_("Pin confirmation"),
        required=True,
    )

    phone_number = forms.CharField(label='Phone Number', max_length=45, required=True)
    email = forms.EmailField(label=_('Email Address'), required=True)
    #validate_phone = forms.BooleanField(label=_('Phone Number'), required=False)

    country_code = forms.ChoiceField(choices=[(str(i.code), i.name) for i in countries])

    class Meta:
        model = User
        fields = (
            "email", "password", 'password2', 'username', 'phone_number', 'first_name', 'last_name',
            'country_code')
        field_classes = {}
        widgets = {
            'phone_number': IntlTelInputWidget(),
            'country_code': CountrySelectWidget()
        }

    def clean_password(self):
        pin1 = str(self.cleaned_data['password'])
        if len(pin1) != 4:
            raise forms.ValidationError("Pin must be 4 digits")
        return str(pin1)


    def clean_password2(self):
        password1 = str(self.cleaned_data.get("password"))
        password2 = str(self.cleaned_data.get("password2"))

        if len(password2) != 4:
            raise forms.ValidationError("Confirm Pin must be 4 digits")

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return str(password2)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)

        super().__init__(*args, **kwargs)

        if self._meta.model.EMAIL_FIELD in self.fields:
            self.fields[self._meta.model.EMAIL_FIELD].widget.attrs.update({'autofocus': True})

    # def clean_validate_phone(self):
    #     validate_phone = self.cleaned_data.get("validate_phone")
    #     if validate_phone == None or validate_phone == False:
    #         raise forms.ValidationError(
    #             'Phone number is wrong',
    #         )
    #     return validate_phone


    def clean_email(self):
        email = self.cleaned_data['email']
        obj = User.objects.filter(email=email).first()
        if obj:
            raise forms.ValidationError('User with this email is already exists.Try a new email')
        return email

    def _post_clean(self):
        super()._post_clean()

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        user.username = self.cleaned_data["username"]
        user.email = self.cleaned_data["email"]
        user.is_active = True
        if commit:
            user.save()

        return user


class AuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    email/password logins.
    """
    email = forms.EmailField(label=_("Email"), required=True)
    password = forms.IntegerField(
        label=_("Password"),
        required=True,
    )

    error_messages = {
        'invalid_login': _(
            "Please enter a correct %(email)s and pin. Note that both "
            "fields may be case-sensitive."
        ),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)

        # Set the max length and label for the "email" field.
        self.email_field = UserModel._meta.get_field(UserModel.EMAIL_FIELD)
        self.fields['email'].max_length = self.email_field.max_length or 254
        if self.fields['email'].label is None:
            self.fields['email'].label = capfirst(self.email_field.verbose_name)


    def clean_password(self):
        pin1 = str(self.cleaned_data['password'])
        if len(pin1) != 4:
            raise forms.ValidationError("Pin must be 4 digits")
        return str(pin1)


    def clean(self):
        email = self.cleaned_data.get('email')
        password = str(self.cleaned_data.get('password'))

        if email is not None and password:

            username = self.get_user2(email.lower())

            print(username)

            self.user_cache = authenticate(self.request, username=username, password=password)

            if self.user_cache is None:
                for mess in self.get_invalid_login_error():
                    messages.error(self.request, _(mess))

                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    @staticmethod
    def get_user2(email):
        try:
            return User.objects.filter(email=email).first()
        except User.DoesNotExist:
            return None

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user(self):
        return self.user_cache

    def get_invalid_login_error(self):
        return forms.ValidationError(
            self.error_messages['invalid_login'],
            code='invalid_login',
            params={'email': self.email_field.verbose_name},
        )
