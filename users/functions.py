def getUserData(user):
    lists = {}
    lists['first_name'] = user.first_name
    lists['last_name'] = user.last_name
    lists['email'] = user.email
    lists['phone_number'] = user.phone_number
    return lists


def checkPhoneNumber(request):
    from rest_framework.response import Response
    from users.models import Users as User
    from rest_framework import status

    data = request.data
    phone_number = data.get('phone_number')
    if not data or not phone_number:
        return Response({'success': False, 'message': 'Phone number is required'}, status=status.HTTP_400_BAD_REQUEST)

    user = User.objects.filter(phone_number=phone_number).first()
    if not user:
        return Response({'success': False, 'message': 'Phone number is wrong'}, status=status.HTTP_400_BAD_REQUEST)

    return Response({'success': True, 'user': getUserData(user)}, status=status.HTTP_200_OK)


