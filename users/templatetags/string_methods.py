from django import template

register = template.Library()


@register.filter()
def uppercase(str):
    return str.upper()

@register.filter()
def lowercase(str):
    return str.lower()
