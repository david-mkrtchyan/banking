from users.models import Users as User
from django.utils.translation import gettext as _
from django.contrib.auth.hashers import make_password
from django_countries import countries
from utils.models import Accounts, Currencies, TransactionSend
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from django.db import DatabaseError, transaction
from django import forms
from twilio.rest import Client
from datetime import date
from utils.models import Variables
from random import randint
from utils.seeds import Seeds
from utils.models import ExchangeRate, TransactionsExchange
from rest_framework.fields import (empty)

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'phone_number',)




class TransactionSendSerializer(serializers.Serializer):
    phone_number = serializers.CharField(label='Phone Number', max_length=45, required=True)
    account_id = serializers.ChoiceField(choices=[(str(i.id), str(i.id)) for i in Accounts.objects.all()],
                                         required=True)
    amount = serializers.DecimalField(required=True, decimal_places=2, max_digits=10)
    message = serializers.CharField(required=True, max_length=5000)

    def __init__(self, data=empty, **kwargs):
        self.request = kwargs.pop('request', None)
        super(TransactionSendSerializer, self).__init__(self, data=data, **kwargs)

    def validate_phone_number(self, value):
        obj = User.objects.filter(phone_number=value).first()
        if not obj:
            raise serializers.ValidationError('Phone number is wrong')
        return value

    def validate(self, data):
        """
        Check that the start is before the stop.
        """
        account_id = data.get('account_id')
        message = data.get('message')
        phone_number = data.get('phone_number')
        amount = int(data.get('amount'))

        fromaccount = Accounts.objects.filter(id=account_id).first()
        if not fromaccount:
            raise serializers.ValidationError(
                _("Account does not exists"),
                code='wrong_account',
            )

        fromUser = self.request.user
        touser = User.objects.filter(phone_number=phone_number).first()
        toaccount = touser.getAccount(fromaccount.currency_id)

        if not fromaccount:
            raise serializers.ValidationError(
                _("Currency is wrong"),
                code='wrong_currency',
            )

        if fromaccount.balance < amount:
            raise serializers.ValidationError(
                _(f"Your amount less than {str(amount)}"),
                code='less_balance',
            )

        if fromUser.id == touser.id:
            raise serializers.ValidationError(
                _(f"You cant't make a transaction on your account"),
                code='less_balance',
            )

        try:
            with transaction.atomic():

                if not toaccount:
                    toaccount = Accounts.objects.create(
                        currency_id=fromaccount.currency_id,
                        user_id=touser.id,
                        active=True,
                    )

                toaccount.balance += amount
                toaccount.save()

                fromaccount.balance -= amount
                fromaccount.save()

                TransactionSend.objects.create(
                    currency_id=fromaccount.currency_id,
                    from_user_id=fromUser.id,
                    from_account_id=fromaccount.id,
                    to_user_id=touser.id,
                    to_account_id=toaccount.id,
                    amount=amount,
                    message=message,
                )
        except DatabaseError as e:
            pass

        return data


class LatestTransactionSerializer(serializers.Serializer):
    currency = serializers.ChoiceField(choices=[(str(i.name), str(i.name)) for i in Currencies.objects.all().only("name")],
                                            required=True)

    def __init__(self, data=empty, **kwargs):
        self.request = kwargs.pop('request', None)
        super(LatestTransactionSerializer, self).__init__(self, data=data, **kwargs)

    def validate(self, data):
        request = self.request
        currency = data.get('currency')
        transactions = request.user.getUserTransactions(currency)
        lists = []

        if transactions:
            for i in transactions:
                data = {
                    'amount': i.amount,
                    'timestamp': i.timestamp.strftime('%d.%m.%Y'),
                }

                if i.from_user_id == request.user.id:
                    data['user_name'] = f'{str(i.to_user.first_name)} {str(i.to_user.last_name)}'
                    data['currency'] = i.from_account.currency.name
                    data['currency_symbol'] = i.from_account.currency.symbol
                    data['send_or_receive'] = '-'
                else:
                    data['user_name'] = f'{str(i.from_user.first_name)} {str(i.from_user.last_name)}'
                    data['currency'] = i.to_account.currency.name
                    data['currency_symbol'] = i.to_account.currency.symbol
                    data['send_or_receive'] = '+'

                lists.append(data)

        return lists

class ExchangeSerializer(serializers.Serializer):
    base_currency = serializers.ChoiceField(choices=[(str(i.name), str(i.name)) for i in Currencies.objects.all().only("name")],
                                            required=True)

    swap_currency = serializers.ChoiceField(choices=[(str(i.name), str(i.name)) for i in Currencies.objects.all().only("name")],
                                            required=True)

    amount = serializers.DecimalField(required=True, decimal_places=2, max_digits=10)

    def __init__(self, data=empty, **kwargs):
        self.request = kwargs.pop('request', None)
        self.getData = kwargs.pop('getData', None)
        super(ExchangeSerializer, self).__init__(self, data=data, **kwargs)

    def validate(self, data):
        import math
        import decimal

        base_currency_raw = data.get('base_currency')
        swap_currency_raw = data.get('swap_currency')
        amount = data.get('amount')

        base_currency = Currencies.getByName(base_currency_raw)
        swap_currency = Currencies.getByName(swap_currency_raw)

        if base_currency.id == swap_currency.id:
            return {}

        exchange_rate = ExchangeRate.objects.filter(base_currency_id=base_currency.id,
                                                    swap_currency_id=swap_currency.id).first()

        if not exchange_rate:
            raise serializers.ValidationError(
                _(f"We can't exchange from your {base_currency_raw} account into {swap_currency_raw} account"),
                code='phone_pin',
            )

        account_base_currency = Accounts.objects.filter(currency_id=base_currency.id,
                                                        user_id=self.request.user.id).first()
        account_swap_currency = Accounts.objects.filter(currency_id=swap_currency.id,
                                                        user_id=self.request.user.id).first()

        if not account_base_currency or not account_swap_currency:
            raise serializers.ValidationError(
                _(f"You can't make exchange, something went wrong"),
                code='base_currency_and_swap_currency_wrong',
            )

        if account_base_currency.balance < amount:
            raise serializers.ValidationError(
                _(f"Your amount less than {str(amount)}"),
                code='less_balance',
            )

        exchangeMargin = base_currency.exchange_margin if math.isclose(base_currency.exchange_margin,
                                                                       swap_currency.exchange_margin) == True else swap_currency.exchange_margin

        appExchange = float("{0:.6f}".format(exchange_rate.exchange_rate * exchangeMargin))

        price = decimal.Decimal("{0:.2f}".format(decimal.Decimal(amount) * decimal.Decimal(appExchange)))

        if self.getData:
            return {
                'price': price,
                'appExchange': appExchange,
            }

        try:
            with transaction.atomic():
                account_swap_currency.balance += price
                account_swap_currency.save()

                account_base_currency.balance -= decimal.Decimal(amount)
                account_base_currency.save()

                TransactionsExchange.objects.create(
                    base_currency=base_currency,
                    swap_currency=swap_currency,
                    exchange_rate=exchange_rate.exchange_rate,
                    exchange_margin=exchangeMargin,
                    app_exchange=appExchange,
                    from_amount=amount,
                    to_amount=price,
                    user=self.request.user,
                )
        except DatabaseError as e:
            pass

        return data



class CheckPinSerializer(serializers.Serializer):
    phone_pin = serializers.IntegerField(
        label=_("Phone Pin"),
        required=True,
    )

    pin = serializers.IntegerField(
        label=_("Pin"),
        required=True,
    )

    pin2 = serializers.IntegerField(
        label=_("Pin confirmation"),
        required=True,
    )

    phone_number = serializers.CharField(label='Phone Number', max_length=45, required=True)

    def validate_pin(self, value):
        pin1 = str(value)
        if len(pin1) != 4:
            raise serializers.ValidationError("Pin must be 4 digits")
        return str(pin1)

    def validate_pin2(self, value):
        pin1 = str(value)
        if len(pin1) != 4:
            raise serializers.ValidationError("Pin confirmation must be 4 digits")
        return str(pin1)

    def validate_phone_pin(self, value):
        pin1 = str(value)
        if len(pin1) != 4:
            raise serializers.ValidationError("Phone Pin must be 4 digits")
        return str(pin1)

    def validate(self, data):

        phone_number = data.get('phone_number')
        phone_pin = data.get('phone_pin')

        user = User.objects.filter(phone_number=phone_number, pin=phone_pin).first()
        if not user:
            raise serializers.ValidationError(
                _("Phone pin is not correct"),
                code='phone_pin',
            )

        pin = str(data.get('pin'))
        pin2 = str(data.get('pin2'))

        if pin and pin2 and pin != pin2:
            raise serializers.ValidationError(
                _("The two pin fields didn't match."),
                code='pin2',
            )

        user.pin = pin
        user.phone_verified = True
        user.save()

        return data


class UserSerializerRegister(serializers.ModelSerializer):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    password = serializers.CharField(
        write_only=True,
        required=True,
        min_length=8,
        label=_("Password"),
        help_text='',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )

    password2 = serializers.CharField(
        write_only=True,
        required=True,
        min_length=8,
        label=_("Password confirmation"),
        help_text='',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )

    phone_number = serializers.CharField(label='Phone Number', max_length=45, required=True)
    email = serializers.EmailField(label=_('Email Address'), required=True)

    country_code = serializers.CharField(max_length=255)

    def __init__(self, instance=None, data=empty, **kwargs):
        self.request = kwargs.pop('request', None)
        self.ip_add = self.request.META.get('REMOTE_ADDR')
        super(UserSerializerRegister, self).__init__(instance=None, data=data, **kwargs)

    def validate_password(self, password1):
        MIN_LENGTH = 8

        # At least MIN_LENGTH long
        if len(password1) < MIN_LENGTH:
            raise forms.ValidationError("The new password must be at least %d characters long." % MIN_LENGTH)

        # At least one letter and one non-letter
        first_isalpha = password1[0].isalpha()
        if all(c.isalpha() == first_isalpha for c in password1):
            raise forms.ValidationError("The password must contain at least one letter and at least one digit or" \
                                        " punctuation character.")

        return password1

    def validate_password2(self, password2):
        MIN_LENGTH = 8

        # At least MIN_LENGTH long
        if len(password2) < MIN_LENGTH:
            raise forms.ValidationError("The password confirmation must be at least %d characters long." % MIN_LENGTH)

        # At least one letter and one non-letter
        first_isalpha = password2[0].isalpha()
        if all(c.isalpha() == first_isalpha for c in password2):
            raise forms.ValidationError(
                "The password confirmation must contain at least one letter and at least one digit or" \
                " punctuation character.")

        return password2

    def validate(self, data):
        """
        Check that the start is before the stop.
        """

        user = User.objects.filter(ip_address=self.ip_add, date_joined__day=date.today().day).all()
        bLock_ip_address = Variables.byKey(Seeds.bLock_ip_address)

        if bLock_ip_address:
            if len(user) > int(bLock_ip_address.value):
                raise serializers.ValidationError(
                    _("You send a lot of requests, we can't register your account"),
                    code='lot_of_request',
                )

        password1 = str(data.get('password'))
        password2 = str(data.get('password2'))

        if password1 and password2 and password1 != password2:
            raise serializers.ValidationError(
                _("The two pin fields didn't match."),
                code='password_mismatch',
            )

        return data

    def create(self, validated_data):

        phone_number = User.objects.filter(phone_number=validated_data['phone_number']).first()

        if phone_number:
            try:
                same_phone_number_mes = Variables.byKey(Seeds.the_same_phone_number_verify)
                if same_phone_number_mes:
                    sendMessageToPhone(validated_data["phone_number"], same_phone_number_mes.value)
            except:
                pass

            return False

        try:
            del validated_data['password2']
            validated_data['password'] = make_password(validated_data['password'])
            user = super().create(validated_data)

            Currencies.createModel(user.id)

            try:
                code = str(randint(1000, 9999))
                code_after_register = Variables.byKey(Seeds.code_after_register)
                message = f'{code_after_register.value}'.format(code=code)
                if message:
                    sendMessageToPhone(validated_data["phone_number"], message)
                    user.pin = code
            except:
                pass

            user.ip_address = self.ip_add

            user.save()

            return user

        except Exception as e:
            pass

    class Meta:
        model = User
        fields = ("email", "password", 'password2', 'phone_number', 'first_name', 'last_name', 'country_code')


def sendMessageToPhone(phone_numer, message, from_='WYRIFY'):
    account_sid = 'ACdb149218f7580bcde7cd02315459c672'
    auth_token = 'fc6ce639ced07d18a041e1496a23fe47'
    client = Client(account_sid, auth_token)

    client.messages \
        .create(
        body=message,
        from_=from_,
        to=f'{phone_numer}'
    )


class UserSerializerWithToken(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)

    def get_token(self, obj):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(obj)
        token = jwt_encode_handler(payload)
        return token

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('token', 'password')
