from django.db import models
from django.contrib.auth.models import AbstractUser
from django_countries.fields import CountryField
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.utils.translation import gettext_lazy as _


# Create your models here.

class Users(AbstractUser):
    list_display = ('email', 'first_name', 'last_name', 'phone_number', 'is_staff',)
    # add another user columns
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        null=True,
        blank=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )

    phone_number = models.CharField(max_length=45, null=True, blank=True, unique=True)
    country_code = CountryField(blank_label='(select country)', null=True, blank=True)
    phone_verified = models.BooleanField(default=False)
    pin = models.IntegerField(null=True, blank=True)
    ip_address = models.GenericIPAddressField(null=True, blank=True)

    USERNAME_FIELD = 'phone_number'

    class Meta:
        swappable = 'AUTH_USER_MODEL'
        db_table = 'auth_user'

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    @property
    def getAccounts(self):
        from utils.models import Accounts
        return Accounts.objects.filter(user_id=self.pk).all()

    def getUserTransactions(self, currency = None):
        from utils.models import TransactionSend
        from utils.models import Currencies
        from django.db.models import Q

        query = TransactionSend.objects.filter(Q(from_user_id=self.pk) | Q(to_user_id=self.pk))
        if currency:
            currencyModel = Currencies.getByName(currency)
            query = query.filter(currency_id=currencyModel.pk)
        query = query.order_by('timestamp').all()
        return query

    def getAccount(self, currencyID):
        from utils.models import Accounts
        return Accounts.objects.filter(user_id=self.pk, currency_id=currencyID).first()
