from . import views
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [

    path('register/', views.UserRegister.as_view()),

    path('api-auth/', include('rest_framework.urls')),

    path('token-auth/', views.TokenObtainPairView.as_view(), name='token_obtain_pair'),

    path('token-auth/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]

# IsAuthenticated routes
urlpatterns += [
    path('user-currencies/', views.UserCurrencies.as_view()),
    path('check-user/', views.CheckUserExist.as_view()),
    path('current-user/', views.CurrentUser.as_view()),
    path('transaction-send/', views.TransactionSend.as_view()),
    path('check-pin/', views.CheckPin.as_view()),
    path('exchange/', views.Exchange.as_view()),
    path('exchange-data/', views.ExchangeGetData.as_view()),
    path('user-feed/', views.UserFeed.as_view()),
    path('latest-transaction/', views.LatestTransaction.as_view()),
]
