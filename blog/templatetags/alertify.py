from django import template

register = template.Library()


@register.inclusion_tag('templatetags/alertify_content.html')
def alertify_content(form, messages):
    return {'form': form, 'messages': messages}
