import os
from celery import Celery
from banking import settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'banking.settings')

app = Celery('tasks', backend='db+sqlite:///results.sqlite', broker='pyamqp://guest@localhost//')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(settings.INSTALLED_APPS)