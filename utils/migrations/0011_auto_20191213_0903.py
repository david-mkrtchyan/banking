# Generated by Django 2.2.7 on 2019-12-13 09:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0010_auto_20191213_0825'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exchangerate',
            name='timestamp',
            field=models.DateTimeField(),
        ),
    ]
