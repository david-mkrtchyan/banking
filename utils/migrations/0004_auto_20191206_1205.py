# Generated by Django 2.2.7 on 2019-12-06 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0003_variables'),
    ]

    operations = [
        migrations.AlterField(
            model_name='variables',
            name='key',
            field=models.SlugField(unique=True),
        ),
    ]
