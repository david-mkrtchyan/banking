# Generated by Django 2.2.7 on 2019-12-16 12:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0011_auto_20191213_0903'),
    ]

    operations = [
        migrations.AddField(
            model_name='currencies',
            name='exchange_margin',
            field=models.FloatField(default=0.05),
        ),
    ]
