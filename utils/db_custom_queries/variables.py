
def select_variable(conn, key):
    """
    Query table by fund_name and db_name
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()

    sql = f"select * from utils_variables where key = '{key}'"
    cur.execute(sql)

    return cur.fetchone()


def getVariableByKey(key):
    import psycopg2
    from utils.db_custom_queries.db_connection import connection

    with connection():
        return select_variable(connection(), key)

def get_axes_failure_limit(key):
    return  int(getVariableByKey(key)[2]) if getVariableByKey(key) else 10

def get_axes_time(key):
    return  int(getVariableByKey(key)[2]) if getVariableByKey(key) else 1
