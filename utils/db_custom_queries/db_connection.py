import sqlite3
from sqlite3 import Error
dbs = []

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def connection():
    import psycopg2
    from banking.settings import DATABASES
    DB_SETING = DATABASES['default']

    try:
        conn = psycopg2.connect(host=DB_SETING['HOST'], database=DB_SETING['NAME'], user=DB_SETING['USER'], password=DB_SETING['PASSWORD'])
        return conn
    except Error as e:
        print(e)

    return None

