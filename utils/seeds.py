
from .models import Variables, VariableType, EmailTemplate


class Seeds:

    bLock_ip_address = 'bLock_ip_address'
    bLock_ip_address_for_login = 'bLock_ip_address_for_login'
    the_same_phone_number_verify = 'the_same_phone_number_verify'
    code_after_register = 'code_after_register'
    blocked_registration_failure_by_hours = 'blocked_registration_failure_by_hours'
    rate_fluctuation_percent = 'rate_fluctuation_percent'
    blocked_account = 'blocked_account'
    exchange_rate_phone = 'exchange_rate_phone'
    exchange_rate_text = 'exchange_rate_text'

    email_template_block_account = 'blocked-account-notification'

    def __init__(self):
        pass

    @staticmethod
    def saveDb():


        exchange_rate_phone = Variables.objects.filter(key=Seeds.exchange_rate_phone).first()
        if not exchange_rate_phone:
            Variables.objects.create(
                name='Phone number variable in message for Exchange rate difference',
                value='+447916242918',
                type=VariableType.text.value,
                key='exchange_rate_phone',
                multiple=False,
            )

        exchange_rate_text = Variables.objects.filter(key=Seeds.exchange_rate_text).first()
        if not exchange_rate_text:
            Variables.objects.create(
                name='Exchange rate difference',
                value='Lorem Ipsum is simply dummy text of the printing and typesetting industry',
                type=VariableType.text.value,
                key='exchange_rate_text',
                multiple=False,
            )


        from banking.settings import AXES_COOLOFF_TIME

        blocked_account = Variables.objects.filter(key=Seeds.blocked_account).first()
        if not blocked_account:
            text = 'hours' if AXES_COOLOFF_TIME > 1 else 'hour'
            Variables.objects.create(
                name='Blocked account notification',
                value=f'Account is blocked the next {str(AXES_COOLOFF_TIME)} {text}. Contact the admin to unlock your account.',
                type=VariableType.text.value,
                key='blocked_account',
                multiple=False,
            )


        rate_fluctuation_percent = Variables.objects.filter(key=Seeds.rate_fluctuation_percent).first()

        if not rate_fluctuation_percent:
            Variables.objects.create(
                name='Rate fluctuation by x percent',
                value='20',
                type=VariableType.number.value,
                key='rate_fluctuation_percent',
                multiple=False,
            )


        email_template_block_account = EmailTemplate.objects.filter(slug=Seeds.email_template_block_account).first()

        if not email_template_block_account:
            EmailTemplate.objects.create(
                name='Blocked account notification',
                subject='[site_name]: Account Blocked',
                description='<p>Account is blocked the next one hour. Contact the admin to unlock your account.</p>',
                slug=Seeds.email_template_block_account,
            )

        bLock_ip_address = Variables.objects.filter(key=Seeds.bLock_ip_address).first()

        if not bLock_ip_address:
            Variables.objects.create(
                name='BLock IP address after x requests to register',
                value='5',
                type=VariableType.number.value,
                key='bLock_ip_address',
                multiple=False,
            )

        bLock_ip_address_for_login = Variables.objects.filter(key=Seeds.bLock_ip_address_for_login).first()
        if not bLock_ip_address_for_login:
            Variables.objects.create(
                name='BLock IP address after x failed login requests',
                value='10',
                type=VariableType.number.value,
                key='bLock_ip_address_for_login',
                multiple=False,
            )

        blocked_registration_failure_by_hours = Variables.objects.filter(key=Seeds.blocked_registration_failure_by_hours).first()
        if not blocked_registration_failure_by_hours:
            Variables.objects.create(
                name='IP address is blocked temporarily due to registration failure.You can retry in x hour',
                value='1',
                type=VariableType.number.value,
                key='blocked_registration_failure_by_hours',
                multiple=False,
            )

        the_same_phone_number_verify = Variables.objects.filter(key=Seeds.the_same_phone_number_verify).first()
        if not the_same_phone_number_verify:
            Variables.objects.create(
                name='In case of repeated phone number after registration',
                value='Someone tried to sign up to Wyrify using your phone number. A new user was not created. Please login to your existing account or reset your password.',
                type=VariableType.text.value,
                key='the_same_phone_number_verify',
                multiple=False,
            )


        code_after_register = Variables.objects.filter(key=Seeds.code_after_register).first()
        if not code_after_register:
            Variables.objects.create(
                name='Send code after register',
                value='Your code is {code}',
                type=VariableType.text.value,
                key='code_after_register',
                multiple=False,
            )
