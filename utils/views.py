from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from .seeds import Seeds
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.decorators import login_required
from utils.models import Images
import json
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
import requests
import datetime
from users.serializers import sendMessageToPhone
from utils.models import Currencies, ExchangeRate, Variables

# Create your views here.

@user_passes_test(lambda u: u.is_superuser)
def seed(request):
    Seeds.saveDb()

    return JsonResponse({'imported': True})


@user_passes_test(lambda u: u.is_superuser)
@login_required
def removeImage(request, id):
    if request.method == 'GET':
        get_object_or_404(Images, id=id).delete()
        return JsonResponse({'sucess': True})
    return JsonResponse({'sucess': False})


@user_passes_test(lambda u: u.is_superuser)
@login_required
def imageSort(request):
    stacks = request.POST.get('stack')
    if request.method == 'POST' and stacks:
        jsonData = json.loads(stacks)
        for key, value in enumerate(jsonData):
            Images.objects.filter(id=int(value['key'])).update(rank=int(key + 1))

    return JsonResponse({'sucess': True})


@user_passes_test(lambda u: u.is_superuser)
@login_required
@csrf_exempt
def imageDelete(request):
    id = request.POST.get('key')
    if request.method == 'POST' and id:
        Images.objects.filter(id=int(id)).delete()
    return JsonResponse({'sucess': True})

