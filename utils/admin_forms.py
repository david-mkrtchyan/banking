from django import forms
from .models import Currencies, CurrencyType, Accounts, TransactionSend, Variables, TransactionsExchange

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from utils.models import VariableType
from .models import EmailTemplate


class CurrencyForm(forms.ModelForm):
    type = forms.ChoiceField(choices=[(i.value, i.name) for i in CurrencyType], required=True)


    def __init__(self, *args, **kwargs):
        super(CurrencyForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Currencies
        exclude = []


class AccountsForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AccountsForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Accounts
        exclude = []


class TransactionSendForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TransactionSendForm, self).__init__(*args, **kwargs)

    class Meta:
        model = TransactionSend
        exclude = []



class TransactionsExchangeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TransactionsExchangeForm, self).__init__(*args, **kwargs)

    class Meta:
        model = TransactionsExchange
        exclude = []

class VariableAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(VariableAdminForm, self).__init__(*args, **kwargs)

        instance = kwargs.get('instance')
        if instance and instance.pk:
            if int(instance.type) == int(VariableType.textArea.value):
                self.fields['value'] = forms.CharField(widget=CKEditorUploadingWidget())
            elif int(instance.type) == int(VariableType.image.value):
                self.fields['value'].required = False
            elif int(instance.type) == int(VariableType.select.value):
                values = instance.choice_values.split(',')
                if instance.multiple == True:

                    selected = instance.value.split(',')

                    self.fields['value'] = forms.MultipleChoiceField(
                        choices=[(i, i) for i in values],
                        initial=(c for c in selected),
                        widget=forms.CheckboxSelectMultiple()
                    )

                    self.initial['value'] = [i for i in selected ]

                else:
                    self.fields['value'] = forms.ChoiceField(choices=[(i, i) for i in values])
                    self.initial['value'] = instance.value



    class Meta:
        model = Variables
        exclude = []


class EmailTemplateForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = EmailTemplate
        exclude = []
