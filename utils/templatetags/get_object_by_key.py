from django import template

register = template.Library()

@register.simple_tag
def get_obj(obj, key):
    return obj.get(key)
