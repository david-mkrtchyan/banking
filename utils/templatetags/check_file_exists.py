from django import template
from banking.settings import BASE_DIR
register = template.Library()
import os

@register.simple_tag
def exists(path):

    if not path:
        return False

    return True if os.path.exists(BASE_DIR+path) else False