from django.contrib import admin
from .models import Currencies, CurrencyType, Accounts, TransactionSend, Variables, VariableType, ExchangeRate, TransactionsExchange
from .admin_forms import CurrencyForm, AccountsForm, TransactionSendForm, VariableAdminForm, EmailTemplateForm, TransactionsExchangeForm
from django.utils.safestring import mark_safe
from utils.models import Images
from utils.templatetags.file_exists import file_exists
from django.urls import reverse
from utils.functions import handle_uploaded_file
# Register your models here.
from utils.models import ImageType, EmailTemplate
from slugify import slugify
from django.utils.crypto import get_random_string


class CurrenciesAdmin(admin.ModelAdmin):
    list_filter = ('name', 'type', 'active', 'auto', 'exchange_margin','symbol')

    def currency_type(self):
        currency = [i.name for i in CurrencyType if i.value == self.type]
        if currency:
            return mark_safe(currency[0])

    currency_type.short_description = 'Type'

    list_display = ('name', currency_type, 'active', 'auto','exchange_margin', 'symbol')

    form = CurrencyForm

    def save_model(self, request, obj, form, change):
        obj.save()

        super(CurrenciesAdmin, self).save_model(request, obj, form, change)

        from utils.management.commands.get_currencies import sendRequestToFixer
        sendRequestToFixer(obj.name)


class ExchangeRateAdmin(admin.ModelAdmin):
    list_display = ('base_currency', 'swap_currency', 'exchange_rate', 'timestamp')


class AccountsAdmin(admin.ModelAdmin):
    list_filter = ('currency__name', 'user__first_name', 'active',)

    list_display = ('currency', 'balance', 'active', 'user')

    form = AccountsForm


class TransactionSendAdmin(admin.ModelAdmin):
    list_display = ('currency', 'from_user', 'from_account', 'to_user', 'to_account', 'amount', 'message','timestamp')

    form = TransactionSendForm

    actions = None
    fieldsets = [
        (None, {'fields': ()}),
    ]

    def __init__(self, *args, **kwargs):
        super(TransactionSendAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = None

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        actions = super(TransactionSendAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

class TransactionsExchangeAdmin(admin.ModelAdmin):
    list_display = ('base_currency', 'swap_currency', 'exchange_rate', 'exchange_margin', 'app_exchange', 'from_amount', 'to_amount','user', 'timestamp')

    form = TransactionsExchangeForm

    actions = None
    fieldsets = [
        (None, {'fields': ()}),
    ]

    def __init__(self, *args, **kwargs):
        super(TransactionsExchangeAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = None

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        actions = super(TransactionsExchangeAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


class VariableAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')

    fields = ['name', 'value']

    form = VariableAdminForm

    change_form_template = 'admin/variables/change_form.html'

    def __init__(self, *args, **kwargs):
        super(VariableAdmin, self).__init__(*args, **kwargs)
        # self.list_display_links = None

    def save_model(self, request, obj, form, change):

        if int(obj.type) == int(VariableType.select.value):
            import ast
            if obj.multiple == True:
                l = ast.literal_eval(obj.value)
                obj.value = ','.join(l)

        elif int(obj.type) == int(VariableType.image.value):

            images = request.FILES.getlist('images')

            if images:
                for image in images:
                    writenFile = handle_uploaded_file(image)
                    if writenFile:
                        Images.objects.create(
                            object_id=obj.id,
                            type=ImageType.variable,
                            path=writenFile,
                        )

        obj.save()

        super(VariableAdmin, self).save_model(request, obj, form, change)

    def change_view(self, request, object_id, form_url='', extra_context=None):

        extra_context = extra_context or {}

        imagesObject = Images.objects.filter(object_id=object_id, type=ImageType.variable).order_by('rank')
        images = imagesConfig = ''

        if imagesObject:
            for image in imagesObject:
                images += '"' + str(file_exists(image.get_path(image))) + '",'
                imagesConfig += "{'url': '" + reverse('image-delete') + "', 'key':'" + str(
                    image.id) + "', 'extra' : {'key':'" + str(image.id) + "','type': 'posts'} },"

        extra_context['images'] = str(images)
        extra_context['imagesConfig'] = imagesConfig

        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        actions = super(VariableAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    class Media:
        js = (
            '/static/admin/js/main.js',
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js',
            '/static/node_modules/bootstrap-fileinput/js/plugins/piexif.js',
            '/static/node_modules/bootstrap-fileinput/js/plugins/sortable.js',
            '/static/node_modules/bootstrap-fileinput/js/plugins/purify.js',
            '/static/node_modules/bootstrap-fileinput/js/fileinput.js',
        )
        css = {
            'all': (
                'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
                '/static/node_modules/bootstrap-fileinput/css/fileinput.min.css',
                '/static/admin/css/style.css',
                '/static/admin/css/fileinput_custom.css',
            )
        }


class EmailTemplateAdmin(admin.ModelAdmin):
    form = EmailTemplateForm
    list_display = ('name', 'subject', 'description',)
    fields = ['name', 'subject', 'description']

    def save_model(self, request, obj, form, change):
        if change == False:
            slug = slugify(str(obj.name))
            obj.slug = slug + get_random_string(length=10) if obj.checkslug(slug) else slug

        obj.save()

        super(EmailTemplateAdmin, self).save_model(request, obj, form, change)


admin.site.register(ExchangeRate, ExchangeRateAdmin)
admin.site.register(EmailTemplate, EmailTemplateAdmin)
admin.site.register(Variables, VariableAdmin)
admin.site.register(Currencies, CurrenciesAdmin)
admin.site.register(Accounts, AccountsAdmin)
admin.site.register(TransactionSend, TransactionSendAdmin)
admin.site.register(TransactionsExchange, TransactionsExchangeAdmin)
