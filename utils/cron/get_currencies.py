from django_cron import CronJobBase, Schedule
from django.db.models import Q
import requests
import datetime
from users.serializers import sendMessageToPhone
from utils.models import Currencies, ExchangeRate, Variables
from utils.seeds import Seeds

class GetCurrencies(CronJobBase):
    jobByMinutes = Variables.byKey(Seeds.update_currencies_by_cron_job)

    RUN_EVERY_MINS =  1 if jobByMinutes else 1

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'get-currencies'

    def do(self):

        from utils.models import Currencies

        currencies = Currencies.objects.all()

        if currencies:
            for i in currencies:
                sendRequestToFixer(i.name)


access_key = 'ffba61598af3a898620c4e46334badbd'
URL = "http://data.fixer.io/api/latest"

def sendRequestToFixer(currencyName):

    variablePercent = Variables.byKey(Seeds.rate_fluctuation_percent)


    percent = int(variablePercent.value) if variablePercent else 20

    currencies = Currencies.objects.filter(~Q(name=currencyName)).all().values_list('name', flat=True)

    swapCurrencies = ",".join(str(curr) for curr in currencies)

    PARAMS = {
        'format': 1,
        'access_key': access_key,
        'base': currencyName,
        'symbols': swapCurrencies,
    }

    r = requests.get(url=URL, params=PARAMS)

    data = r.json()

    if data:
        basecurr = Currencies.getByName(data['base'])
        if basecurr:
            for key, val in data['rates'].items():
                swapcurr = Currencies.getByName(key)
                if swapcurr:
                    exists = ExchangeRate.objects.filter(base_currency_id=basecurr.id,
                                                         swap_currency_id=swapcurr.id).first()
                    if exists:
                        lastperc = exists.exchange_rate * percent / 100

                        if val < (exists.exchange_rate - lastperc) or val > (exists.exchange_rate + lastperc):
                            try:
                                exchange_rate_phone = Variables.byKey(Seeds.exchange_rate_phone)
                                exchange_rate_text = Variables.byKey(Seeds.exchange_rate_text)
                                if exchange_rate_phone and exchange_rate_text:
                                    sendMessageToPhone(exchange_rate_phone.value, exchange_rate_text.value)
                            except:
                                pass

                            return False

                        exists.exchange_rate = val
                        exists.timestamp = datetime.datetime.fromtimestamp(int(data['timestamp']))
                        exists.save()
                    else:
                        ExchangeRate.objects.create(
                            base_currency_id=basecurr.id,
                            swap_currency_id=swapcurr.id,
                            exchange_rate=val,
                            timestamp=datetime.datetime.fromtimestamp(int(data['timestamp'])),
                        )