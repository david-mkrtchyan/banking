from django.db import models
from enum import Enum
from users.models import Users as User
from django.contrib.postgres.fields import JSONField
from django_enumfield import enum
# Create your models here.
from enum import Enum
from PIL import Image
import secrets
import os
from django.utils import timezone
from banking.settings import BASE_DIR
from utils.functions import dir_exists

typeFlat = 1
typeCrypto = 2


class CurrencyType(Enum):
    Flat = typeFlat
    Crypto = typeCrypto


class Currencies(models.Model):
    name = models.CharField(max_length=255,unique=True)
    type = models.SmallIntegerField(default=CurrencyType.Flat)
    active = models.BooleanField(default=True)
    auto = models.BooleanField(default=True)
    symbol = models.CharField(max_length=255, null=False, blank=False, default='$')
    exchange_margin = models.FloatField(null=False,blank=False, default=0.05)

    def __str__(self):
        return str(self.name)

    @staticmethod
    def createModel(userID):

        currencies = Currencies.objects.filter(auto=True).all()

        if currencies:
            for currency in currencies:
                Accounts.objects.create(
                    user_id=userID,
                    currency=currency,
                    active=True,
                )

    @staticmethod
    def getByName(name):
        return Currencies.objects.filter(name=name).first()


class ExchangeRate(models.Model):
    base_currency = models.ForeignKey(Currencies, on_delete=models.CASCADE)
    swap_currency = models.ForeignKey(Currencies, on_delete=models.CASCADE, related_name='swap_currency_to_currency')
    exchange_rate = models.FloatField(null=False, blank=False)
    timestamp = models.DateTimeField()

    def __str__(self):
        return str(self.base_currency)

    class Meta:
        db_table = 'exchange_rates'

class TransactionsExchange(models.Model):
    base_currency = models.ForeignKey(Currencies, on_delete=models.CASCADE)
    swap_currency = models.ForeignKey(Currencies, on_delete=models.CASCADE, related_name='swap_currency_to_currency_exchange')
    exchange_rate = models.FloatField(null=False, blank=False)
    exchange_margin = models.FloatField(null=False, blank=False)
    app_exchange = models.FloatField(null=False, blank=False)
    from_amount = models.DecimalField(null=False, blank=False, decimal_places=2, max_digits=10)
    to_amount = models.DecimalField(null=False, blank=False, decimal_places=2, max_digits=10)
    user = models.ForeignKey(User,on_delete=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user.phone_number)

    class Meta:
        db_table = 'transactions_exchange'


class Accounts(models.Model):
    currency = models.ForeignKey(Currencies, on_delete=models.CASCADE, null=False, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
    active = models.BooleanField(default=True)
    balance = models.DecimalField(null=False, blank=False, default=0.00, decimal_places=2, max_digits=10)

    def __str__(self):
        return str(self.user.phone_number)


class TransactionSend(models.Model):
    currency = models.ForeignKey(Currencies, on_delete=models.CASCADE, null=False, blank=False)
    from_user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
    from_account = models.ForeignKey(Accounts, on_delete=models.CASCADE, null=False, blank=False)
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False,
                                related_name='to_user_transaction')
    to_account = models.ForeignKey(Accounts, on_delete=models.CASCADE, null=False, blank=False,
                                   related_name='to_account_transaction')
    amount = models.DecimalField(null=False, blank=False, decimal_places=2, max_digits=10)
    message = models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_created=True, auto_now_add=True)

    def __str__(self):
        return str(self.from_user)


class VariableType(Enum):
    number = 1
    text = 2
    textArea = 3
    image = 4
    select = 5


class Variables(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    value = models.TextField(null=True, blank=True)
    type = models.SmallIntegerField(default=VariableType.number)
    key = models.SlugField(null=False, blank=False, unique=True)
    multiple = models.BooleanField(default=False)
    choice_values = models.TextField(null=True, blank=True, )

    @property
    def getTypeName(self):
        return '\n'.join(str(i.name) for i in VariableType if i.value == self.type)

    @staticmethod
    def byKey(key):
        return Variables.objects.filter(key=key).first()

    def __str__(self):
        return str(self.name)


# Create your models here.
type_variable = 1


class ImageType(enum.Enum):
    variable = type_variable


class ImagesTrait(object):

    @property
    def getImages(self):
        return Images.objects.filter(type=self.imageType, object_id=self.id).order_by('-rank').all()

    @property
    def getImage(self):
        return Images.objects.filter(type=self.imageType, object_id=self.id).order_by('-rank').first()

    @property
    def getImagePath(self, size='big'):
        objImage = self.getImage
        if objImage:
            return Images.get_path(objImage, size)


class EmailTemplate(models.Model):
    slug = models.SlugField(unique=True)

    name = models.CharField(max_length=255)

    description = models.TextField()

    subject = models.CharField(max_length=255)

    @staticmethod
    def checkslug(slugname):
        return EmailTemplate.objects.filter(slug=slugname).first()

    def __str__(self):
        return str(self.name)

    class Meta:
        db_table = 'email_template'


class Images(models.Model):
    path = models.ImageField()
    object_id = models.IntegerField()
    type = models.SmallIntegerField(default=ImageType.variable)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)
    rank = models.IntegerField(default=0, null=True)

    def save(self, *args, **kwargs):

        if not self.path:
            super().save(*args, **kwargs)

        elif self.path:
            img = Image.open(self.path.path)

            random_hex = secrets.token_hex(8)
            _, f_ext = os.path.splitext(img.filename)
            picture_fn = random_hex + f_ext

            bigsize = f'{BASE_DIR}/media/uploads/big/{self.type}/'
            dir_exists(bigsize)

            picture_path = os.path.join(bigsize, picture_fn)
            img.save(picture_path)
            if os.path.isfile(self.path.path):
                os.remove(self.path.path)
                self.path = f'{picture_fn}'
                super().save(*args, **kwargs)

            if img.width > 465 or img.height > 300:
                smallsize = f'{BASE_DIR}/media/uploads/small/{self.type}/'
                dir_exists(smallsize)
                picture_path = os.path.join(smallsize, picture_fn)
                output_size = (465, 300)
                img.resize(output_size, Image.ANTIALIAS)
                img.save(picture_path, quality=100)

    @staticmethod
    def get_path(obj, size='big'):
        return f'/media/uploads/{size}/{obj.type}/{obj.path}'

    class Meta:
        db_table = 'images'
